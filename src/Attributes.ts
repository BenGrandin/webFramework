class Attributes<T> {
	constructor(private data: T) { }

	// 165. Advanced Generic Constraints
	get = <K extends keyof T>(key: K): T[K] => {
		return this.data[key];
	};

	set = (update: T): void => {
		this.data = { ...this.data, ...update };
	};

	getAll = (): T => {
		return this.data;
	};
}


export { Attributes };